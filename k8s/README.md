
# Solution diagram

![Diagram](diagram.png)

# ELK Instance

A managed node group is set to generate ELK compute instance as it manage the auto-scaling group and other compute bootstrap for us.
We just have to choose the right AMI ("amazon-eks-node-v*") and set their IAM role accordingly (we can directly use the AmazonEKSClusterPolicy and AmazonEKSServicePolicy)
These instances can be reached only through a LoadBalancer with an associated security group. We could also use an SSH bastion - not represented here - if ever we need to reach them through a ssh tunnel.

# Redis and postgresql service

The Redis and Postgresql service can be instanciated using Amazon Elasticache and Amazon RDS for Postgresql.
These two services are instanciated in a dedicated private VPC (without Internet gateway).
A VPC peering is etablished between the two VPC. Their route tables have to be set accordingly, and redis/postgresql security group must allow the EKS workers CIDR.

# Front load balancer

The front load-balancer is associated with the Nginx Ingress Controller kubernetes service during its deployment.
