# Dockerfile for production
FROM node:erbium-alpine as build

WORKDIR /app
RUN mkdir -p /app

COPY src /app/src
COPY package* /app/
COPY tsconfig* /app/

# Install strictly following package-lock
RUN npm ci
RUN npm run build

FROM node:erbium-alpine as api

# Prepare workdir
RUN addgroup -g 2000 nodejs && adduser -u 2000 -D -G nodejs nodejs
RUN mkdir -p /app

WORKDIR /app

# Copy package and lock
COPY package* /app/
COPY tsconfig* /app/

# Copy build result in app directory
COPY --from=0 /app/dist/ /app/
COPY --from=0 /app/node_modules /app/node_modules

# Chown /app folder
RUN chown nodejs:root -R /app

# Start node server (Downgrade to non-privileged user)
USER nodejs
EXPOSE 3000
CMD node main.js
